#!/usr/bin/python3

import configparser
import requests
import sys
import os.path

configFile = "./vultr-firewall-cloudflare.config"

# check that the conifg file exists
if not os.path.isfile(configFile):
    print("Your config file does not exist.\nPlease create ./vultr-firewall-cloudflare.config using ./vultr-firewall-cloudflare.config.example as a guide")
    sys.exit()


config = configparser.ConfigParser()
config.read(configFile)
vultrAPIkey = config.get('general', 'vultrAPIkey')


def getCloudFlareIPs(version):
    IPs = requests.get('https://www.cloudflare.com/ips-' + str(version))
    if IPs.status_code == 200:
        return IPs.text
    else:
        print("There was an error getting the CloudFlare IPs")
        print("Response code : " + str(IPs.status_code))
        sys.exit()


def checkForFirewallGroup():
    rqheaders = {'API-Key': vultrAPIkey}
    firewallGroupName = "cloudflare_and_ssh"

    rqurl = "https://api.vultr.com/v1/firewall/group_list"
    firewallGroups = requests.get(rqurl, headers=rqheaders)

    # check that the API request worked, if not display the error
    if firewallGroups.status_code != 200:
        print("There was an error connecting to the Vultr API.")
        print("Response code : " + str(firewallGroups.status_code))
        sys.exit()

    firewallGroups = firewallGroups.json()

    # check if there is already a firewall group created and return the ID
    for firewallGroup in firewallGroups:
        if firewallGroups[firewallGroup]["description"] == firewallGroupName:
            fwID = firewallGroups[firewallGroup]["FIREWALLGROUPID"]
            return fwID

    # now we know the firewall group doesn't exist create it and return the ID
    rqurl = "https://api.vultr.com/v1/firewall/group_create"
    rqdata = {'description': firewallGroupName}
    firewallID = requests.post(rqurl, headers=rqheaders, data=rqdata)
    fwID = firewallID.json()['FIREWALLGROUPID']
    print("Created firewall group : " + fwID)
    return fwID


def whitelistIPs(ip_type, firewallID, IPs):
    count = 0
    countfail = 0
    # split the line at the newline
    lines = IPs.split("\n")

    for IP in lines:
        # check if the line contains a "/"  CIDR IP address. If it does then add it to the firewall group
        if '/' in IP:
            whitelist = whitelistSingleIP(ip_type, firewallID, IP, "1-65535")

            # check the response code and ensure it was a 200    
            if whitelist == 200:
                count += 1
            else:
                countfail += 1

    # whitelist SSH IPv4 from everywhere
    if ip_type == 'v4':
        whitelist = whitelistSingleIP('v4', firewallID, "0.0.0.0/0", "22")
        if whitelist == 200:
            count += 1
        else:
            countfail += 1

    # if we have created, of failed to create rules then print the details
    if count > 0:
        print(str(count) + " IP" + ip_type + " firewall rules created")
    if countfail > 0:
        print(str(countfail) + " IP" + ip_type + " firewall rules FAILED to be created")
    return


def whitelistSingleIP(ip_type, firewallID, IP, port):
    rqheaders = {'API-Key': vultrAPIkey}
    rqurl = "https://api.vultr.com/v1/firewall/rule_create"
    ipPart, subnetPart = IP.split("/")

    # whitelisting the cloudflare IP for all ports, assuming no attack will come from these IPs
    rqdata = {'FIREWALLGROUPID': firewallID, 'direction': 'in', 'ip_type': ip_type, 'protocol': 'tcp', 'subnet': str(ipPart), 'subnet_size': str(subnetPart), 'port': port}
    whitelisted = requests.post(rqurl, headers=rqheaders, data=rqdata)
    return whitelisted.status_code


def deleteCurrentRules(firewallID, ip_type):
    count = 0
    countfail = 0
    rqheaders = {'API-Key': vultrAPIkey}
    rqurl = "https://api.vultr.com/v1/firewall/rule_list"
    rqdata = {'FIREWALLGROUPID': firewallID, 'direction': 'in', 'ip_type': ip_type}
    firewallRules = requests.get(rqurl, headers=rqheaders, params=rqdata).json()

    # parse the returned firewall rules and delete them
    # it would be good if vultr could add a comment field, then we could be sure we were only delting rules we created
    rqurl = "https://api.vultr.com/v1/firewall/rule_delete"
    for firewallRule in firewallRules:
        rqdata = {'FIREWALLGROUPID': firewallID, 'rulenumber': firewallRules[firewallRule]["rulenumber"]}
        ruleDeleted = requests.post(rqurl, headers=rqheaders, data=rqdata)

        # check that we got a 200 success back
        if ruleDeleted.status_code == 200:
            count += 1
        else:
            countfail += 1

    # if we havecreated, of failed to create rules then print the details
    if count > 0:
        print(str(count) + " IP" + ip_type + " firewall rules deleted")
    if countfail > 0:
        print("FAILED to delete " + str(countfail) + " firewall rules")


# get the correct firewall group, or if there isn't one create it
firewallID = checkForFirewallGroup()
print("Working with vultr firewall ID : " + firewallID)

# get the list of current IPs from CloudFlare
IPsv4 = getCloudFlareIPs("v4")
IPsv6 = getCloudFlareIPs("v6")

# check that we got both IPv4 and IPv6 information back. If not then we don't want to delete the current rules
if (not IPsv4) or (not IPsv6):
    print("We appear to be missing IPs from CloudFlare. Script will terminate now")
    sys.exit()

# delete the current rules and then add the current IP rules
deleteCurrentRules(firewallID, 'v4')
deleteCurrentRules(firewallID, 'v6')
whitelistIPs("v4", firewallID, IPsv4)
whitelistIPs("v6", firewallID, IPsv6)
