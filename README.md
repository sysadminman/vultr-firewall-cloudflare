# README #
  
This python script will create a firewall group called "cloudflare_and_ssh" using the vultr API. It will then whitelist the current cloudflare IPs. SSH (port 22) is also globally whitelisted in the firewall.  
You should run this script to create a firewall group, and then assign your webserver to that group  

Requires Python v3

### How do I get set up? ###
  
git clone git@bitbucket.org:sysadminman/vultr-firewall-cloudflare.git  
create a vultr-firewall-cloudflare.config using vultr-firewall-cloudflare.config.example as a guide. This will contain your vultr API key  
./vultr-firewall-cloudflare.py  

The add your vultr server to the new firewall group called "cloudflare_and_ssh"  

### Who do I talk to? ###
  
bb@sysadminman.net